<?php
$nom = "Calatraba";
$prenom = "Vincent";
$login = "CalatrabaV";

$vincent = [
    'prenom' => $prenom,
    'nom'    => $nom,
    'login'  => $login
];
$cyprien = [
    'prenom' => 'Cyprien',
    'nom'    => 'Bons',
    'login'  => 'BonsC'
];
$mael = [
    'prenom' => 'Mael',
    'nom'    => 'Nicolas',
    'login'  => 'MaelN'
];

$utilisateur = [
    '1' => $vincent,
    '2' => $cyprien,
    '3' => $mael,
    '' => 'Il n’y a aucun utilisateur.'
];



echo "Utilisateur ".$prenom." ".$nom." de login ".$login."\n";
print_r($utilisateur);
echo "Utilisateur ".$utilisateur['1']['prenom']." ".$utilisateur['1']['nom']." de login ".$utilisateur['1']['login']."\n";
echo $utilisateur['']."\n";
?>
