<?php
class Utilisateur {

    private String $login;
    private String $nom;
    private String $prenom;

    public function getNom() : String {
        return $this->nom;
    }

    public function getLogin() : String {
        return $this->login;
    }

    public function getPrenom() : String {
        return $this->prenom;
    }

    public function setNom(String $nom) {
        $this->nom = substr($nom, 0, 64);
    }

    public function setLogin(String $login) {
        $this->login = substr($login, 0, 64);
    }

    public function setPrenom(String $prenom) {
        $this->prenom = substr($prenom, 0, 64);
    }

    public function __construct(String $login, String $nom, String $prenom) {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function __toString() : String {
        return "Utilisateur ".$this->prenom." ".$this->nom." de login ".$this->login."\n";
    }
}
?>
